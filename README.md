# Bact.io Readme

## Tech

* [node.js](https://nodejs.org/en/) - evented I/O for the backend
* [Express](https://expressjs.com/) - fast node.js network app framework
* [MongoDB](https://www.mongodb.com) - MongoDB stores data in flexible, JSON-like documents
* [Mongoose.js](https://mongoosejs.com/) - Mongoose is an Object Data Modeling (ODM) library for MongoDB and NodeJS
* [Phaser.js](https://phaser.io/) - A fast, fun and free open source HTML5 game framework

## Setup instructions

* Install Node.js
* Install MongoDB
	* Edit package.json > script > Change to your data folder for mongo (--dbpath ~/mongo/data/db) in prestart 

```sh
$ npm install -d
```

## How to run

To start the server: 

```sh
$ cd project_directory
$ npm strat
```

When you finish, simply press CTRL+C and shut down MongoDB gracefully with:

```sh
$ npm stop
```